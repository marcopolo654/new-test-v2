const express = require("express");
const cors = require("cors");

function main() {
    const app = express();

    app.use(cors());
	
	app.get("/test", (req, res) => {
        res.json({ 
            text: "test"
        });
    });

    app.get("/hello", (req, res) => {
        const a = [];

        for (let i = 0; i < 30000; i++) {
            a.push({
                i,
                a: "a",
                b: "b"
            });
        }

        res.json({ 
            text: "world",
            a
        });
    });

    app.get("/error", () => {
        null.t();
    });

    const port = process.env.PROT || 8080;
    app.listen(port, () => { 
        console.log(`Server is running at port: ${port}`);
    });
}

main();